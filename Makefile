#=======================================================================
# SE1   - Sistemas Embebidos 1
#-----------------------------------------------------------------------
# Turma:	LI51N
# Semestre:	Inverno 2010/2011
# Data:		Novembro/2010
#-----------------------------------------------------------------------
# Nome: 	Nuno Cancelo
# Numero:	31401
#-----------------------------------------------------------------------
# Nome:		Nuno Sousa
# Numero:	33595
#-----------------------------------------------------------------------
# LEIC  - Licenciatura em Engenharia Informática e Computadores
# DEETC - Dep. de Eng. Electrónica e Telecomunicações e Computadores
# ISEL  - Instituto Superior de Engenharia de Lisboa
#=======================================================================
#											Base Dir MakeFile
#=======================================================================
#Shell Path
SHELL			= /bin/sh
#Environmental Paths
#=======================================================================
PROJECT_PATH	= .
#Phony Target
.PHONY 				= clean
.PHONY				= clear
.PHONY				= debugger
.PHONY				= peripherical
.PHONY				= device
.PHONY				= program
.PHONY				= deploy
.PHONY				= collection
#=======================================================================
include $(PROJECT_PATH)/common.mk
#=======================================================================
LDS_PATH		=  	$(TODEPLOY)/ldscript
OPENOCD_FILE		=	$(TODEPLOY)/openocd/openocd_lpc2106.cfg

#Set the Correct LdScript
LDSCRIPT_FILE		= $(ECOS_LIBRARY)/target.ld
END_EXEC		= $(TARGET)/program.elf

VPATH 			= $(LIB):$(CLIB):$(libdir)
#=======================================================================
#LYBRARIES = 		-lLPC2106
LYBRARIES = 		
OBJECT =		$(LIB)/ENC28J60.o $(LIB)/ETHERNET.o $(UIP_LPC)/obj/main.o $(UIP_LPC)/obj/tapdev.o $(UIP_LPC)/obj/psock.o


#Program to be compiled
PCOMPILE			= $(PROGRAM)/
#=======================================================================
all: $(END_EXEC) 

#Generate the executable
#$(OBJECT)
$(END_EXEC): program 
#	$(LD) $(LDSCRIPT) $(LDSCRIPT_FILE) $(OUTPUT) $@  -nostdlib $(PROGRAM)/test_uIP.o $(OBJECT) $(SEARCHLIB) -lLPC2106 -luip -lapps
	$(LD) $(LDFLAGS) $(LDSCRIPT) $(LDSCRIPT_FILE)  $(OUTPUT) $@   $(OBJECT) $(SEARCHLIB) $(LYBRARIES) -lapps -luip 

program: 
	@echo "----------------------------------------------------------------------"
	$(MAKE) -C $(SOURCE)
	$(MAKE) -C $(PCOMPILE)

#clean files
clean:
	@echo "----------------------------------------------------------------------"

	$(MAKE) -C $(SOURCE) 		clean
	$(MAKE) -C $(PCOMPILE) 		clean

#Deploy executable to LPC
deploy: $(END_EXEC)
	$(OPENOCD) -f $(OPENOCD_FILE) -c "flash write_image erase $^" -c "sleep 100" -c "reset run" -c "shutdown"

clear:
	clear
