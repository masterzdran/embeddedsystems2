#=======================================================================
# SE1   - Sistemas Embebidos 1
#-----------------------------------------------------------------------
# Turma:	LI51N
# Semestre:	Inverno 2010/2011
# Data:		Novembro/2010
#-----------------------------------------------------------------------
# Nome: 	Nuno Cancelo
# Numero:	31401
#-----------------------------------------------------------------------
# Nome:		Nuno Sousa
# Numero:	33595
#-----------------------------------------------------------------------
# LEIC  - Licenciatura em Engenharia Informática e Computadores
# DEETC - Dep. de Eng. Electrónica e Telecomunicações e Computadores
# ISEL  - Instituto Superior de Engenharia de Lisboa
#=======================================================================
#=======================================================================
SOURCE				= $(PROJECT_PATH)/src
COLLECTION		= $(SOURCE)/collection
DEVICE				= $(SOURCE)/device
PERIPHERICAL	= $(SOURCE)/peripherical
DEBUGGER			= $(SOURCE)/debugger
TODEPLOY			= $(SOURCE)/deploy
PROGRAM				= $(SOURCE)/applications

#=======================================================================
PECOS					= $(SOURCE)/ecos/install/ecos2_install
#=======================================================================
PUIP					= $(SOURCE)/uip-1.0/
#=======================================================================
TARGET				= $(PROJECT_PATH)/target
TEST					= $(PROJECT_PATH)/test
#=======================================================================
LIB						= $(SOURCE)/lib
ECOS_LIBRARY  = $(PECOS)/lib
UIP_LIBRARY		= $(PUIP)/lpc-2106

INCLUDE				= $(SOURCE)/include
ECOS_INCLUDE	= $(PECOS)/include
UIP_INCLUDE		= $(PUIP)/uip
ECOS_TESTS		= $(PCOMPILE)/ecos
UIP_LPC				= $(PUIP)/lpc-2106
HEADER				= 
SEARCHINCLUDE	= -I$(ECOS_INCLUDE) -I$(INCLUDE) -I$(UIP_INCLUDE) -I$(UIP_LPC)
SEARCHLIB			= -L$(UIP_LIBRARY) -L$(ECOS_LIBRARY) -L$(LIB)
VPATH 				= $(LIB):$(ECOS_LIBRARY)

#Executables
CC 				= arm-eabi-gcc
LD 				= arm-eabi-gcc
AS				= arm-eabi-gcc
#AS				= arm-eabi-as
#LD				= arm-eabi-ld
AR				= arm-eabi-ar rcs
OPENOCD			= openocd
#Debugger
#DEBUGGER		= arm-eabi-insight
#Commom Options
OUTPUT 				= -nostdlib  -o
LDSCRIPT			= -T  
DEBUGSTABS			= --gstabs
DEBUGSYMBOLS		= -g
COPTIONS 			= -Wall  -ffunction-sections -fdata-sections -fpack-struct -Wa,-a=$*.lst $(SEARCHINCLUDE)
LDFLAGS 			= -Wl,--gc-sections -Wl,--Map -Wl,basic.map $(SEARCHLIB)
COMPILE_ONLY 		= -c 






