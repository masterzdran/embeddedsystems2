#ifndef CYGONCE_PKGCONF_DEVS_SPI_ARM_LPC2XXX_H
#define CYGONCE_PKGCONF_DEVS_SPI_ARM_LPC2XXX_H
/*
 * File <pkgconf/devs_spi_arm_lpc2xxx.h>
 *
 * This file is generated automatically by the configuration
 * system. It should not be edited. Any changes to this file
 * may be overwritten.
 */

#define CYGPKG_DEVS_SPI_ARM_LPC2XXX_BUS0 1
#define CYGNUM_IO_SPI_ARM_LPC2XXX_BUS0_INTPRIO 12
#define CYGNUM_IO_SPI_ARM_LPC2XXX_BUS0_INTPRIO_12

#endif
